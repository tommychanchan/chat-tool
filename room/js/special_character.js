function special_character(text) {
	var re="";
	for (var i=0; i<text.length; i++) {
		switch (text.charAt(i)) {
			case " ":
				re+="+";
				break;
			case "+":
				re+="%2B";
				break;
			case "!":
				re+="%21";
				break;
			case "%":
				re+="%25";
				break;
			case "=":
				re+="%3D";
				break;
			case "'":
				re+="%27";
				break;
			case ";":
				re+="%3B";
				break;
			case ":":
				re+="%3A";
				break;
			case "`":
				re+="%60";
				break;
			case "@":
				re+="%40";
				break;
			case "#":
				re+="%23";
				break;
			case "$":
				re+="%24";
				break;
			case "^":
				re+="%5E";
				break;
			case "&":
				re+="%26";
				break;
			case "(":
				re+="%28";
				break;
			case ")":
				re+="%29";
				break;
			case "/":
				re+="%2F";
				break;
			case "?":
				re+="%3F";
				break;
			case "\\":
				re+="%5C";
				break;
			case "|":
				re+="%7C";
				break;
			case "[":
				re+="%5B";
				break;
			case "]":
				re+="%5D";
				break;
			case "{":
				re+="%7B";
				break;
			case "}":
				re+="%7D";
				break;
			case ",":
				re+="%2C";
				break;
			default:
				re+=text.charAt(i);
		}
	}
	return re;
}
