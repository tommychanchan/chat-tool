<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php require 'pname.php'; echo "$pname"; ?></title>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="bookmark" href="favicon.ico"/>

<meta name="viewport" content="width=device-width, initial-scale=1" />

<style>

</style>

</head>
<body>

<?php
header("Content-Type: text/html; charset=UTF-8");
require 'pname.php';
session_start();
$tit=$_POST['tit'];
$tit = htmlspecialchars($tit);

if (!ctype_alpha($tit)) {
	echo "<script>alert('聊天室名稱不可包含 空格、符號、數字、其他語言文字，只可以包含英文字母。');window.location='create.php';</script>";
	return false;
}

if ($tit=="" || $tit==null)
{
	echo "<script>alert('請填上聊天室名稱。');window.location='create.php';</script>";
	return false;
}

$cn = mysqli_connect($dhost, $dname, $dpass, $droom);
if (!$cn) {
	die("Connection failed: " . mysqli_connect_error());
}

mysqli_set_charset($cn,"utf8");

$sql="CREATE TABLE `$droom`.`$tit` ( `id` INT NOT NULL AUTO_INCREMENT , `username` TEXT NOT NULL , `role` TEXT NOT NULL , `power` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";

$result = mysqli_query($cn, $sql) or
	die("<script>alert('此聊天室名稱已經存在。');window.location='create.php';</script>");

$titlogs=$tit . "logs";
$sql="CREATE TABLE `$droom`.`$titlogs` ( `id` INT NOT NULL AUTO_INCREMENT , `username` TEXT NOT NULL , `msg` TEXT NOT NULL , `date` TEXT NOT NULL , `time` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";

$result = mysqli_query($cn, $sql) or
	die("<script>alert('此聊天室名稱已經存在。');window.location='create.php';</script>");



$uname=$_SESSION['username'];
$sql = "INSERT INTO `$tit`
	(`username`, `role`, `power`)
		VALUES
	('$uname', 'owner', '0')";
$result = mysqli_query($cn, $sql) or
	die("Error!");


$con = mysqli_connect($dhost, $dname, $dpass, $dchat);
if (!$con) {
	echo 'Unable to connect to server';
}

mysqli_set_charset($con,"utf8");



$val = mysqli_query($con, "select 1 from $uname LIMIT 1");
if($val !== FALSE)
{
	//can have code here.
}
else
{
	$sql="CREATE TABLE `$dchat`.`$uname` ( `id` INT NOT NULL AUTO_INCREMENT , `room` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
	$result = mysqli_query($con, $sql) or
		die("<script>alert('Error!');window.location='create.php';</script>");
}



$sql = "INSERT INTO `$uname`
	(room)
		VALUES
	('" . utf8_encode($tit) . "')";
$result = mysqli_query($con, $sql) or
	die(mysqli_error($con));



echo "<script>alert('成功創建名稱爲 $tit 的聊天室。');window.location='index.php?room=" . $tit . "';</script>";
?>

</body>
</html>
