<?php
require 'pname.php';
session_start();
if(!isset($_SESSION['username'])){
	echo "<script>alert('沒有此用戶！');window.location='../index.php';</script>";
	return false;
}

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

if(isset($_FILES['file'])) {
	$file = $_FILES['file'];
	$room=$_POST['room'];
	$roomlogs=$room . "logs";
	$uname=$_SESSION['username'];
	
	
	$file_name = $file['name'];
	$file_tmp = $file['tmp_name'];
	$file_size = $file['size'];
	$file_error = $file['error'];
	
	$file_ext = explode('.', $file_name);
	$file_ext = strtolower(end($file_ext));
	
	$allowed = array('png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'ico', 'ICO');
	
	if (in_array($file_ext, $allowed)) {
		if ($file_error === 0) {
			if ($file_size <= 9999999) {
				$file_name_new = uniqid('', true) . '.' . $file_ext;
				$file_destination = 'photo/' . $file_name_new;
				
				if (move_uploaded_file($file_tmp, $file_destination)) {
					//success
					$photolink="[PHOTO]" . $file_destination;
					$photolink=encrypt_decrypt('encrypt', $photolink);
					
					//connect database
					$con = mysqli_connect($dhost, $dname, $dpass, $droom);
					if (!$con) {
						echo 'Unable to connect to server';
					}
					
					//add message with photo
					date_default_timezone_set("Asia/Hong_Kong");
					$mydate=getdate(date("U"));
					$date="$mydate[mday]/$mydate[mon]/$mydate[year]";
					$time=date("h:i:sa");
					$sql="INSERT INTO $roomlogs
						(`username`, `msg`, `date`, `time`)
							VALUES
						('$uname', '$photolink', '$date', '$time')";
					$result = mysqli_query($con, $sql) or
						die("Error!");
					
					echo "<script>window.location='index.php?room=" . $room . "';</script>";
				}
				else
				{
					echo "<script>alert('上傳失敗！');window.location = 'index.php?room=" . $room . "';</script>";
				}
			}else{
				echo "<script>alert('你上傳的文件容量太大了，無法上傳！');window.location = 'index.php?room=" . $room . "';</script>";
			}
		} else {
			echo "<script>alert('上傳失敗！');window.location = 'index.php?room=" . $room . "';</script>";
		}
	}else{
		//not image file
		if ($file_error === 0) {
			if ($file_size <= 9999999) {
				$file_name_new = uniqid('', true) . '.' . $file_ext;
				$file_destination = 'file/' . $file_name_new;
				
				if (move_uploaded_file($file_tmp, $file_destination)) {
					//success
					$photolink="[FILE]" . $file_destination;
					$photolink=encrypt_decrypt('encrypt', $photolink);
					
					//connect database
					$con = mysqli_connect($dhost, $dname, $dpass, $droom);
					if (!$con) {
						echo 'Unable to connect to server';
					}
					
					//add message with photo
					date_default_timezone_set("Asia/Hong_Kong");
					$mydate=getdate(date("U"));
					$date="$mydate[mday]/$mydate[mon]/$mydate[year]";
					$time=date("h:i:sa");
					$sql="INSERT INTO $roomlogs
						(`username`, `msg`, `date`, `time`)
							VALUES
						('$uname', '$photolink', '$date', '$time')";
					$result = mysqli_query($con, $sql) or
						die("Error!");
					
					echo "<script>window.location='index.php?room=" . $room . "';</script>";
				}
				else
				{
					echo "<script>alert('上傳失敗！');window.location = 'index.php?room=" . $room . "';</script>";
				}
			}else{
				echo "<script>alert('你上傳的文件容量太大了，無法上傳！');window.location = 'index.php?room=" . $room . "';</script>";
			}
		} else {
			echo "<script>alert('上傳失敗！');window.location = 'index.php?room=" . $room . "';</script>";
		}
	}
} else {
	//no file selected
	$room=$_POST['room'];
	echo "<script>alert('上傳失敗！');window.location='index.php?room=" . $room . "';</script>";
	return;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">

<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="bookmark" href="favicon.ico"/>

<title><?php require 'pname.php'; echo "$pname"; ?></title>
</head>
<body>

</body>
</html>