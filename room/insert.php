﻿<?php
require 'pname.php';
session_start();
$uname = $_SESSION['username'];
$msg = $_REQUEST['msg'];
$room=$_SESSION['room'];
$roomlogs=$room . "logs";

function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}


//$msgprotectxss = htmlentities($msg);
$msgprotectxss = htmlspecialchars($msg);
//echo $msg . "   " . $msgprotectxss;

$con = mysqli_connect($dhost, $dname, $dpass, $droom);
if (!$con) {
	echo 'Unable to connect to server';
}
mysqli_set_charset($con,"utf8");

if (startsWith($msg, "/clear")) {
	$result = mysqli_query($con, "SELECT * FROM $room WHERE username='$uname'");
	while($row = mysqli_fetch_assoc($result))
	{
		if ($row['power']=='0' || $row['power']=='1') {
			//enough power to clear log.
			$sql = "DELETE FROM $roomlogs";

			if (mysqli_query($con, $sql)) {
				//success
				return;
			} else {
				echo "Error: " . mysqli_error($con);
			}
		} else {
			echo "<script>alert('權限不足！'); window.location='index.php';</script>";
			return;
		}
	}
}

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}


$msgprotectxss=encrypt_decrypt('encrypt', $msgprotectxss);

date_default_timezone_set("Asia/Hong_Kong");
$mydate=getdate(date("U"));
$date="$mydate[mday]/$mydate[mon]/$mydate[year]";
$time=date("h:i:sa");
$sql="INSERT INTO $roomlogs
	(`username`, `msg`, `date`, `time`)
		VALUES
	('$uname', '$msgprotectxss', '$date', '$time')";
$result = mysqli_query($con, $sql) or
	die("Error!");



/*
$result1 = mysqli_query($con, "SELECT * FROM " . $roomlogs . " ORDER by id DESC");

while($row = mysqli_fetch_assoc($result1))
{
	echo "<span style='color:#848484;font-size:85%;'>[" . $row['date'] . " " . $row['time'] . "]</span> <span class='uname' style='font-size:130%;'>". $row['username']. "</span>: <span class='msg' style='font-size:130%;'>" . $row['msg']. "</span><br>";
}
*/





//echo "<script>alert('test');</script>";
?>﻿
