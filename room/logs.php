﻿<?php
require 'pname.php';
session_start();

$room=$_SESSION['room'];
$roomlogs=$room . "logs";

function startsWith($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}


$con = mysqli_connect($dhost, $dname, $dpass, $droom);
mysqli_set_charset($con,"utf8");
$result1 = mysqli_query($con, "SELECT * FROM " . $roomlogs . " ORDER by id DESC");


while($row = mysqli_fetch_assoc($result1))
{
	$tempmsg=$row['msg'];
	
	$tempmsg=encrypt_decrypt('decrypt', $tempmsg);
	
	if (startsWith($tempmsg, "www") || startsWith($tempmsg, "http")) {
		if (startsWith($tempmsg, "www")) {
			$tempmsg="<a href='http://" . $tempmsg . "' target='_blank'>" . $tempmsg . "</a>";
		} else {
			$tempmsg="<a href='" . $tempmsg . "' target='_blank'>" . $tempmsg . "</a>";
		}
	}
	
	if (startsWith($tempmsg, "[PHOTO]")) {
		$tempmsg=substr($tempmsg, 7);
		$tempmsg="<a href='" . $tempmsg . "' target='_blank'><img src='" . $tempmsg . "' width='300px' height='300px'></a>";
	}
	
	if (startsWith($tempmsg, "[FILE]")) {
		$tempmsg=substr($tempmsg, 6);
		$tempmsg="<a href='" . $tempmsg . "' download>" . $tempmsg . "</a>";
	}
	
	
	
	echo "<span style='color:#E6E6E6;font-size:85%;'>[" . $row['date'] . " " . $row['time'] . "]</span> <span class='uname' style='font-size:130%;'>". $row['username'] . "</span>: <span class='msg' style='font-size:130%;'>" . $tempmsg . "</span><br>";
}

?>﻿
