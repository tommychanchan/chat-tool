<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title><?php require 'pname.php'; echo "$pname"; ?></title>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="bookmark" href="favicon.ico"/>

<meta name="viewport" content="width=device-width, initial-scale=1" />

<style>
* {
	margin:0;
	background-color:black;
	color:white;
}

#main {
	margin-left:auto;
	margin-right:auto;
	text-align:center;
	width:100%;
	height:800px;
}

.message {
	font-weight:bold;
	color:red;
}
</style>

<script>
function check() {
	var tit=document.getElementById("tit").value;

	if (tit=="" || tit==null)
	{
		document.getElementById("titmessage").innerHTML="請填上聊天室名稱。";
		return false;
	}
}

function ini() {
	document.getElementById("tit").focus();
}
</script>
</head>
<body background="bg.png" onload="ini()">
<div id="main">
<h2><strong>創建聊天室</strong></h2>
<p></p>
<br/>
<p></p>
<form action="create2.php" method="post" onsubmit="return check()">
聊天室名稱：
<input type="text" id="tit" name="tit" /><br/>
<span id="titmessage" class="message"></span><br/>
<input type="submit" value="創建" />
</form>
<p></p>
<br/>
<p></p>
<a href="../index.php">首頁</a>
</div>
</body>
</html>
