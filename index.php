﻿<?php
require 'pname.php';
session_start();
if(!isset($_SESSION['username'])){
?>
<head>
	<title><?php require 'pname.php'; echo "$pname"; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script>
	function inilogin() {
		document.getElementById("username").focus();
	}
	
	function checkForm() {
		var un = document.getElementById("username").value;
		var pw = document.getElementById("password").value;
		
		if (un == "" || un == null) {
			alert("請填上用戶名！");
			return false;
		}
		
		if (pw == "" || pw == null) {
			alert("請填上密碼！");
			return false;
		}
		
		return true;
	}
	</script>
</head>
<body background="bg.png" onload="inilogin()">
	<form name="form2" action="login.php" method="post" onsubmit="return checkForm()">
		<table border="1px white solid" align="center">
			<tr>
				<td style="color:white;">用戶名：</td>
				<td><input type="text" id="username" name="username" style="background-color:black;color:white;" required></td>
			</tr>
			<tr>
				<td style="color:white;">密碼：</td>
				<td><input type="password" id="password" name="password" style="background-color:black;color:white;" required></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="登入" style="background-color:black;color:white;"></td>
			<tr>
				<td colspan="2"><a href="register.php" style="color:white;">註冊</a></td>
			</tr>
		</table>
	</form>
</body>
<?php
	exit;
}
?>

<html>
	<head>
		<meta charset="utf-8">
		<title><?php require 'pname.php'; echo "$pname"; ?> - <?php echo $_SESSION['username']; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" href="css/global.css"/>
		<style>
		* {
			background-color:black;
			color:white;
		}

		hr {
			background-color:white;
			color:white;
		}
		</style>
		<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
	</head>
	<body background="bg.png">
		<div id="headerr">
			<span style="font-size:200%;color:white;">
				<?php require 'pname.php'; echo "$pname"; ?> - <?php echo $_SESSION['username']; ?>
			</span>
		</div>
		<p></p>

		<span style="font-size:130%;color:white;">歡迎回來，<strong><?php echo $_SESSION['username']; ?></strong></span>
		<hr>
		<br/>
		<a href="room/create.php" style="color:lightblue; font-size:150%;">創建新房間</a>
		<br />
		<hr>
		<form action="changepw.php" method="post">
			<h3>更改密碼</h3>
			<input type="password" name="oldpw" placeholder="舊密碼" /><br />
			<input type="password" name="newpw" placeholder="新密碼" /><br />
			<input type="password" name="newpw2" placeholder="再次輸入新密碼" /><br />
			<input type="submit" value="更改" />
		</form>
		<hr>
		<br/>
		<a href="logout.php" style="color:red;">登出</a>
		<br/>
		<p></p>
		<br/>
		<div id="rooms">
			<?php
			require 'pname.php';
			$uname=$_SESSION['username'];
			$con = mysqli_connect($dhost, $dname, $dpass, $dchat);
			if (!$con) {
				echo 'Unable to connect to server';
			}

			$val = mysqli_query($con, "select 1 from $uname LIMIT 1");
			if($val !== FALSE)
			{
				$result = mysqli_query($con, "SELECT * FROM " . $uname . " ORDER by id DESC");
				while($row = mysqli_fetch_assoc($result))
				{
					echo "<a href='room/index.php?room=" . $row['room'] . "'><span style='color:lightgreen;'>" . $row['room'] . "</span></a><br />";
				}
			}
			else
			{
				//no room.
			}

			?>
		</div>

	</body>﻿
</html>
